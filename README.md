# TSoURDt3rd Assets
## General Synopsis

This repository is just a general holding place for all of TSoURDt3rd's assets. 
Whether that's the assets needed for Discord Rich Presence, the Jukebox, or more, it'll all be handled here.
Definitely saves me some space, makes things cleaner, and allows for more optimizations and features on TSoURDt3rd's part.
