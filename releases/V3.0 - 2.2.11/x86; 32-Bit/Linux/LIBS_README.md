Hi! If you're reading this, you're probably wondering how to get SRB2's libraries on Linux, in order to run the game.
Well, look no further than this README file!

Since You're Using Linux, I Assume you Have Some Experience With Compiling, and Things of That Sort.
So Anyways, Let me Tell You the Steps.

Step 1:
You'll need your distrubtion's package manager. For Ubuntu-like Distrubutions, this will be similar to apt, and for others, like Arch, it'll be Pacman, or for Fedora, it'll be dnf. You get the gist.
You'll also need to be root in order for the packages to install in the first place.

Step 2:
You'll need to install the following packages:
	- build-essential
	- libpng-dev
	- zlib1g-dev
	- libsdl2-dev
	- libsdl2-mixer-dev
	- libsdl2_mixer_ext
	- libgme-dev
	- libopenmpt-dev
	- libcurl4-openssl-dev
	- libdiscord-rpc

These packages may have different names on your distrubtion, so make sure to research any packages you can't find.
	- Note That Some Packages on This List, like the Discord and SDL-Mixer-X libraries, Might Not Be Avaliable in Your Distrubution At All!
		- If This is The Case, Try Compile Their Source Code Yourself! The Repositories are Listed on This Custom Builds' Github!

...And That's It! Just run the game from there, and things will be great!
