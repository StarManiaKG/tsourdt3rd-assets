Hi Hi, Fellow Snoopers!
	If you're reading this, you're tresspassing on my property, and stuff.
		That's fine though, I don't really care.

	Anyways, this file contains everything needed for TSoURDt3rd and its features to run.
		That's it, nothing more, nothing less.
		It's just fun stuff that allows me to do even more fun stuff.

	In this file though, I've left some notes for you.
		If you can snoop, then most likely, you can mod.
		Therefore, here are some notes. It may be best to read them.


--[[ NOTES
	CUSTOM TSoURDt3rd VARIABLES YOU CAN USE:
		// GENERAL //
		- tsourdt3rd - Boolean as to Whether or Not You're Using This Custom Build.							(Read)

		- TSoURDt3rd_LoadedExtras - Boolean as to Whether or Not this Add-On is Loaded.						(Read)
		- TSoURDt3rd_NoMoreExtras - Boolean as to Whether or Not the Extra Features are Still Allowed.		(Read+Write)

		- autoloaded - Boolean as to Whether or Not You've Autoloaded Add-Ons Already.						(Read+Write)

		- ForceTimeOver - Boolean as to Whether to Run The Time Over Functions or Not.						(Read+Write)

		// SOUNDS //
		- STAR_JoinSFX - Integer as to What Sound Should Play When Someone Joins a Server.					(Read+Write)
		- STAR_LeaveSFX - Integer as to What Sound Should Play When Someone Leaves a Server.				(Read+Write)
		- STAR_SynchFailureSFX - Integer as to What Sound Should Play When Someone Gets a Synch Failure.	(Read+Write)

		- DISCORD_RequestSFX - Integer as to What Sound Should Play When You Get a Join Request.			(Read+Write)

		// EVENTS //
		- eastermode - Boolean as to Whether or Not Easter Mode is Enabled.									(Read)
			- AllowEasterEggHunt - Boolean as to Whether to Allow Easter Egg Hunts or Not.					(Read+Write)
			- EnableEasterEggHuntBonuses - Boolean as to Whether to Allow Easter Egg Hunt Bonuses Or Not.	(Read+Write)

			- TOTALEGGS - Value that Points Towards the Total Amount of Eggs Avaliable to Collect.			(Read+Write)
			- foundeggs - Value Flag as to What Specific Eggs you Have Collected.							(Read+Write)

			- collectedmapeggs - Value as to How Many Eggs You Have Currently Collected on a Certain Map.	(Read+Write)
			- currenteggs - Value as to How Many Eggs We Have Currently Collected in Total.					(Read+Write)
			- numMapEggs - Value as to How Many Eggs Are Avaliable to Collect on a Certain Map.				(Read+Write)

		// EXTRAS //
		- SpawnTheDispenser - Boolean as to Whether to Spawn the TF2 Dispenser or Not.						(Read+Write)


	AND THAT'S IT!
]]--