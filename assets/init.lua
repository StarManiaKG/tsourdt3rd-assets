--// ERRORS
-- NO TSOURDT3RD? --
if not (tsourdt3rd)
	error("You Need a Custom SRB2 Build - The Story of Uncapped Revengence Discord the 3rd - In Order to Properly Use This File.", 2)
end

--// LOAD SCRIPTS
for _, tsourdt3rd in ipairs({	
	"LUA_MOBJS",
	"LUA_PLAYERS"
})
	dofile(tsourdt3rd)
end
--// AND WE'RE DONE :) //--