--// THIS CONTAINS ALL THE CODE FOR TSoURDt3rd's CUSTOM PLAYER STUFF! //--
--// HERE ARE SOME NOTES //--
/* 
	Normally This Would be Hardcoded, And At First It Was!
	But When It Was, I Found Out It Would Tend To Break Some Mods, Players, Structures, and Overall,
		It Just Made Everything Very Unstable.
	So I Resorted to Just Making Some Lua Scripts for the Stuff, and Just Hardcoded the Variables Needed Instead.
		So, With That Being Said, Here We Are: Electric Boogalo!
		
	END OF THE NOTES
*/

addHook("PlayerThink", function(player)
	---- Variables ----
	local DispenserGoingUp = (netgame and false or true);
	
	local SpeedUp = true;
	if ((netgame)
		or (currenteggs != TOTALEGGS)
		or (currenteggs == TOTALEGGS and not EnableEasterEggHuntBonuses)
		or (TSoURDt3rd_NoMoreExtras))

		SpeedUp = false;
	end
	
	---- Players ----
	if (player.mo and player.mo.valid)
		-- Events --
		-- Easter
		if (SpeedUp)
			if ((player.cmd.forwardmove or player.cmd.sidemove)
				and	(player.normalspeed < player.speed))
				
				player.mo.friction = FRACUNIT;
			else
				player.mo.friction = 29*FRACUNIT/32;
			end
		end
		
		-- Extras --
		-- TF2
		if (DispenserGoingUp)
			if (SpawnTheDispenser)
				P_SpawnMobj(player.mo.x, player.mo.y, player.mo.z, MT_TF2DISPENSER);
				SpawnTheDispenser = false;
			end
		end
	end
end)

--// AND WE'RE DONE :) //--