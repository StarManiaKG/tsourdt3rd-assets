--// THIS CONTAINS ALL THE CODE FOR TSoURDt3rd's CUSTOM MOBJ STUFF! //--
--// HERE ARE SOME NOTES //--
/* 
	Normally This Would be Hardcoded, And At First It Was!
	But When It Was, I Found Out It Would Tend To Break Some Mods.
	So I Resorted to Just Making Some Lua Scripts for the Stuff, and Just Hardcoded the Variables Needed Instead.
		So, With That Being Said, Here We Are! Again!	
		
	END OF THE NOTES
*/

--// SORT THE TABLES
local eggParamaters =
{
	// GFZ1
	{map = 1, color = SKINCOLOR_GREEN, 	 x = 3201,    y = 6794,   z = 103},
	{map = 1, color = SKINCOLOR_CLOUDY,  x = 10685,   y = 3929,   z = 800}, 
	
	// GFZ2
	{map = 2, color = SKINCOLOR_SAPPHIRE,x = -4088,   y = 4797,   z = 2304},
	{map = 2, color = SKINCOLOR_PINK, 	 x = 638,     y = -5506,  z = 896},
	{map = 2, color = SKINCOLOR_CRIMSON, x = -2290,   y = -1749,  z = 256},
	
	// THZ1
	{map = 4, color = SKINCOLOR_PURPLE,	 x = 15359,   y = -8384,  z = 3696},
	{map = 4, color = SKINCOLOR_YELLOW,  x = 6137,    y = -7354,  z = 2080},
	{map = 4, color = SKINCOLOR_BONE, 	 x = 15680,   y = -3971,  z = 2176},
	
	// THZ2
	{map = 5, color = SKINCOLOR_RED,	 x = -22512,  y = -13,    z = 1600},
	{map = 5, color = SKINCOLOR_JET,	 x = -13329,  y = 7840,   z = 1184},
	{map = 5, color = SKINCOLOR_BLUE, 	 x = -11521,  y = 4013,   z = 1120},
	{map = 5, color = SKINCOLOR_ORANGE,  x = -1,  	  y = -1683,  z = 1536},
	
	// DSZ1
	{map = 7, color = SKINCOLOR_COBALT,  x = 3149,    y = 2446,   z = 384},
	{map = 7, color = SKINCOLOR_ROSY, 	 x = 3809,    y = 5120,   z = -2015},
	{map = 7, color = SKINCOLOR_NEON, 	 x = 12755,   y = 14863,  z = -2008},
	{map = 7, color = SKINCOLOR_KETCHUP, x = 19328,   y = 13213,  z = -2240},
	
	// DSZ2
	{map = 8, color = SKINCOLOR_WHITE, 	 x = 16858,   y = -17024, z = 1088},
	{map = 8, color = SKINCOLOR_YOGURT,  x = 22623,   y = -8921,  z = -16},
	{map = 8, color = SKINCOLOR_VAPOR,   x = 4135,    y = 7166,   z = -440},
	
	// CEZ1
	{map = 10, color = SKINCOLOR_COPPER,  x = 2992,   y = 8447,   z = 3900},
	
	// CEZ2
	{map = 11, color = SKINCOLOR_GOLD,    x = -1817,  y = 1976,   z = 7680}
};
if ((eastermode)
	and not (netgame)
	and not (autoloaded)
	and not (TSoURDt3rd_NoMoreExtras))
	
	TOTALEGGS = #eggParamaters;
end

--// RUN THE HOOKS
addHook("MapLoad", function()
	---- Variables ----
	-- Booleans
	local spawneggs = true;
	if ((netgame or multiplayer or TSoURDt3rd_NoMoreExtras) 	// No cheating!!
		or not (eastermode)										// Yes Easter!!
		or (autoloaded)											// Yes Vanilla: Electric Boogalo!!
		or not (AllowEasterEggHunt))							// Yes Hunting!!
		
		spawneggs = false;
	end
	
	---- Objects ----
	-- Events --
	-- Easter Eggs
	if (spawneggs)
		collectedmapeggs, numMapEggs = 0, 0;
		
		for i = 1, TOTALEGGS do
			if ((eggParamaters[i].map != gamemap)	// They're not on the right map... drat!
				or (foundeggs & 1<<i))				// They already found this egg... drat!
				
				continue;							// Skip This Egg...
			end
	
			local eggMobj = P_SpawnMobj(
				eggParamaters[i].x<<FRACBITS,
				eggParamaters[i].y<<FRACBITS,
				eggParamaters[i].z<<FRACBITS, MT_EASTEREGG);
			eggMobj.health = 1<<i;
			eggMobj.color = eggParamaters[i].color;
			eggMobj.shadowscale = FRACUNIT;
			
			numMapEggs = $ + 1;
		end
	end
end)

for _, StopCheating in ipairs({"MobjSpawn", "MobjThinker"}) do
	addHook(StopCheating, function(mobj)
		---- Variables ----
		local DespawnDispenser = (netgame and true or false);
		
		---- Mobjs ----
		if (mobj and mobj.valid)
			-- Extras --
			-- TF2
			if (DespawnDispenser)
				if (mobj.type == MT_TF2DISPENSER) P_KillMobj(mobj, 0, 0); return true; end
			end
		end
	end, MT_TF2DISPENSER)
end

addHook("TouchSpecial", function(special, toucher)
	if ((special and special.valid)
		and (toucher and toucher.valid and toucher.player and toucher.player.valid))
		
		---- Variables ----
		-- Numbers
		local tempeggvar = 0;
	
		---- Mobjs ----
		-- Events --
		-- Easter Eggs
		if (special.type == MT_EASTEREGG)
			-- Check The Toucher
			if ((modeattacking)								// Are you Playing Regularly?
				or (toucher.player.bot)						// Are you a Real Player?
				or (TSoURDt3rd_NoMoreExtras)				// Are you Playing the Vanilla-Ness?
				or (autoloaded)								// Are you Playing the Vanilla-Ness: Electric Boogalo?
				or not (eastermode)							// Are you in the Easter Season?
				or not (AllowEasterEggHunt))				// Are you Hunting for the Eggs?
				
				if (TSoURDt3rd_NoMoreExtras or autoloaded)	// Are you Playing the Vanilla-Ness - Electric Boogalo: Electric Boogalo?
					CONS_Printf(toucher.player, "You'll need to restart the game to collect this egg.");
					S_StartSound(toucher, sfx_lose);
				end
					
				return;
			end
			
			-- Run For The Toucher
			P_SpawnMobj(special.x, special.y, special.z, MT_SPARK);
			S_StartSound(toucher, sfx_ncitem);
			
			-- Increment Values to the Globals
			foundeggs = ($1|special.health)
			for e = 1, TOTALEGGS do
				if (foundeggs & (1<<e)) tempeggvar = $ + 1; end
			end
			collectedmapeggs = $ + 1;
			currenteggs = tempeggvar;
			
			-- Remove the Offending Egg
			P_KillMobj(special, toucher, special);
		end
	end
end, MT_EASTEREGG)

--// AND WE'RE DONE :) //--